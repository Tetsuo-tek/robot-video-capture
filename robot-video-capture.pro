TARGET = RobotAudioCapture.bin
TEMPLATE = app

INCLUDEPATH += ../SpecialK/LibSkCore
include(../SpecialK/LibSkCore/LibSkCore.pri)
include(../SpecialK/LibSkCore/module-src.pri)

INCLUDEPATH += ../SpecialK/LibSkVision
include(../SpecialK/LibSkVision/LibSkVision.pri)
include(../SpecialK/LibSkVision/module-src.pri)

INCLUDEPATH += ../SpecialK/LibSkMultimedia
include(../SpecialK/LibSkMultimedia/LibSkMultimedia.pri)
include(../SpecialK/LibSkMultimedia/module-src.pri)

DEFINES += ENABLE_XML
DEFINES += ENABLE_SSL
DEFINES += ENABLE_HTTP
DEFINES += ENABLE_CV

INCLUDEPATH += /usr/include/opencv4/
INCLUDEPATH += /usr/local/include/opencv4/

SOURCES += \
    main.cpp \
    robotvideocapture.cpp

HEADERS += \
    robotvideocapture.h

DISTFILES += \
    skmake.json
