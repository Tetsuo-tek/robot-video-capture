#include "robotvideocapture.h"

#include <Core/Containers/skarraycast.h>

ConstructorImpl(RobotVideoCapture, SkFlowSat)
{
    vc = nullptr;
    publisher = nullptr;

    capID = -1;
    compression = 0;

    props.enabled = false;
    props.realTimeMode = false;

    setObjectName("RobotVideoCapture");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool RobotVideoCapture::onSetup()
{
    SkCli *cli = skApp->appCli();

    if (cli->isUsed("--gstreamer-source"))
    {
        gStreamerSource = cli->value("--gstreamer-source").toString();
        props.apiReference = VideoCaptureAPIs::CAP_GSTREAMER;
    }

    else
    {
        capID  = cli->value("--device").toInt();
        props.apiReference = VideoCaptureAPIs::CAP_V4L2;
    }

    props.fps =  cli->value("--fps").toInt();
    AssertKiller(props.fps == 0);

    if (!parseSize(objectName(), cli->value("--resolution").data(), &props.resolution))
        return false;

    props.forceFps = cli->isUsed("--force-fps");
    props.forceResolution = cli->isUsed("--force-resolution");

    props.fourCC = cli->value("--fourcc").toString();
    compression = cli->value("--compression").toInt();

    publisher = new SkFlowVideoPublisher(this);
    publisher->setObjectName(this, "Publisher");

    setupPublisher(publisher);

    return true;
}

void RobotVideoCapture::onInit()
{
    vc = new SkImageCapture(this);
    vc->setObjectName(this, "ImageCapture");

    if (gStreamerSource.isEmpty())
        vc->setup(capID, props);

    else
        vc->setup(gStreamerSource.c_str(), props);

    AssertKiller(!vc->open());

    props.fps = vc->getFps();
    props.resolution = vc->getResolution();

    publisher->setup(&f, props.resolution, props.fps, compression);
    publisher->start();

    if (props.forceFps)
        skApp->changeFastZone(vc->getTickTimeMicros());

    ObjectMessage("Capture STARTED"
                  << " - ID: " << capID
                  << " - res: " << props.resolution
                  << " - fps: " << props.fps << " [" << vc->getTickTimeMicros() << " us]"
                  << " - frameRawSize: " << publisher->getRawFrameSize() << " B");
}

void RobotVideoCapture::onQuit()
{
    publisher->stop();
    ObjectMessage("Capture STOPPED: " << capID << " ..");

    if (vc->isOpen())
        vc->close();

    vc->destroyLater();
    vc = nullptr;

    props.enabled = false;

    capID = -1;
    f.clear();
}

void RobotVideoCapture::onFastTick()
{
    if (!vc || !vc->isOpen())
        return;

    if (props.enabled)
    {
        if (!publisher->hasTargets())
        {
            ObjectMessage("Capture tick NOT-ACTIVE");
            props.enabled = false;
            vc->setEnabled(false);

            eventLoop()->changeTimerMode(SK_TIMEDLOOP_SLEEPING);

            return;
        }
    }

    else
    {
        if (publisher->hasTargets())
        {
            ObjectMessage("Capture tick ACTIVE");
            props.enabled = true;
            vc->setEnabled(true);

            if (props.forceFps)
                skApp->changeTimerMode(SK_TIMEDLOOP_RT);

            else
                skApp->changeTimerMode(SK_FREELOOP);
        }

        else
            return;
    }

    f.set(vc->getFrame());
    publisher->tick();
}
