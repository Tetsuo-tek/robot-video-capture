#include "robotvideocapture.h"

SkApp *skApp = nullptr;

int main(int argc, char *argv[])
{
    skApp = new SkApp(argc, argv);

    SkCli *cli = skApp->appCli();

    cli->add("--device",                "-d", "0",          "Setup the v4l-source device to capture");
    cli->add("--fourcc",                "", "MJPG",       "Setup the v4l-source FourCC (ex. MJPG, YUYV, ..)");
    cli->add("--gstreamer-source",      "-g", "",           "Setup the gstreamer-source to capture");
    //cli->add("--print-device-list",     "-l", "",           "Print the available audio-devices list");
    cli->add("--resolution",            "-r", "640x430",    "Setup the frame resolution");
    cli->add("--force-resolution",      "",   "",           "Use selected resolution when this value is NOT supported by device");
    cli->add("--compression",           "-c", "50",         "Setup the jpeg-compression [1, 100]");
    cli->add("--fps",                   "-f", "10",         "Setup the frames per second value");
    cli->add("--force-fps",             "",   "",           "Use selected fps when this value is NOT supported by device");

#if defined(ENABLE_HTTP)
    SkFlowSat::addHttpCLI();
#endif

    skApp->init(5000, 150000, SK_TIMEDLOOP_RT);
    new RobotVideoCapture;

    return skApp->exec();
}
