################################################################################
# Makefile for building: RobotVideoCapture.bin
# GMT-Time: 
################################################################################

TARGET = RobotVideoCapture.bin
CXX = /usr/bin/g++
LINK = /usr/bin/g++
BUILD_PATH = ./build
DEFINES = -DENABLE_SKAPP -DSPECIALK_APPLICATION -DENABLE_AES -DENABLE_XML -DENABLE_HTTP -DENABLE_SSL -DENABLE_CV
CXXFLAGS = -pipe -g -std=gnu++11 -W -fPIC $(DEFINES)
DEL_FILE = rm -vf
DEL_DIR = rmdir -v
CHK_DIR_EXISTS = test -d
MKDIR = mkdir -p
COPY_FILE = cp -vf
COPY_DIR = cp -vf -R
INSTALL_FILE = install -m 644 -vp
INSTALL_PROGRAM = install -m 755 -vp
SYMLINK = ln -vf -s
MOVE = mv -vf
TAR = tar -cvf
COMPRESS = gzip -9f
SED = sed
STRIP = strip
INCPATH = -I/home/roko/Scrivania/develop/SpecialK/LibSkFlat/ -I/home/roko/Scrivania/develop/SpecialK/LibSkCore/ -I/home/roko/Scrivania/develop/SpecialK/LibSkVision/ -I/home/roko/Scrivania/develop/SpecialK/LibSkMultimedia/ -I/usr/include/opencv4 -I/usr/local/include/opencv4 -I/usr/include/eigen3
LIBS = -lpthread -lm -lz -lssl -lcrypto -lopencv_core -lopencv_videoio -lopencv_imgproc -lopencv_imgcodecs -lopencv_xphoto -lopencv_face -lopencv_objdetect -lopencv_calib3d -lopencv_features2d
OBJECTS = \
		./build/skabstractflowsubscriber.o \
		./build/skabstractflowsat.o \
		./build/skflowsat.o \
		./build/skwhitebalancer.o \
		./build/skimageencoder.o \
		./build/skflowvideomjpegpublisher.o \
		./build/skflowcommon.o \
		./build/skflowprotocol.o \
		./build/skabstractflow.o \
		./build/skflowsync.o \
		./build/skabstractworkerobject.o \
		./build/skflowasync.o \
		./build/skabstractserver.o \
		./build/skabstractflatserver.o \
		./build/skflattcpserver.o \
		./build/sktcpserver.o \
		./build/sksslserver.o \
		./build/skwsredistr.o \
		./build/skwsredistrmountpoint.o \
		./build/skhttpservice.o \
		./build/skfsmountpoint.o \
		./build/skflowasynchttpservice.o \
		./build/skabstractflowpublisher.o \
		./build/skflowgenericpublisher.o \
		./build/skmagmawriter.o \
		./build/skabstractmagmaencoder.o \
		./build/skabstractmagmadecoder.o \
		./build/skimagedecoder.o \
		./build/skpsyvenc.o \
		./build/skflowvideomagmapublisher.o \
		./build/skredistrmountpoint.o \
		./build/skflattcpsocket.o \
		./build/sktcpsocket.o \
		./build/skipaddress.o \
		./build/sknetutils.o \
		./build/sksslsocket.o \
		./build/skpostparser.o \
		./build/skwebsocket.o \
		./build/skgenericmountpoint.o \
		./build/skurl.o \
		./build/skflatlocalsocket.o \
		./build/sklocalsocket.o \
		./build/skhttpsocket.o \
		./build/skrawredistrmountpoint.o \
		./build/skabstractflatsocket.o \
		./build/skabstractsocket.o \
		./build/skdeviceredistr.o \
		./build/skhttpstatuscodes.o \
		./build/skwuielement.o \
		./build/skxmlwriter.o \
		./build/skbufferdevice.o \
		./build/skwebpage.o \
		./build/skhttpresponseheaders.o \
		./build/skcookies.o \
		./build/skmultipartredistr.o \
		./build/skpartredistrmountpoint.o \
		./build/skflowvideopublisher.o \
		./build/skimagecapture.o \
		./build/skabstractdevice.o \
		./build/skflatsignal.o \
		./build/skabstractflatdevice.o \
		./build/skflatfile.o \
		./build/skfile.o \
		./build/skattach.o \
		./build/sksignal.o \
		./build/skslot.o \
		./build/skmutexlocker.o \
		./build/skcli.o \
		./build/skmimetype.o \
		./build/skelapsedtime.o \
		./build/skstandalonesignal.o \
		./build/skringbuffer.o \
		./build/skmath.o \
		./build/skdatabuffer.o \
		./build/skdatacrypt.o \
		./build/skeventloop.o \
		./build/skthread.o \
		./build/skimageutils.o \
		./build/skmat.o \
		./build/skfltkgroups.o \
		./build/skfltklayout.o \
		./build/skfltkwindows.o \
		./build/skfltkwidgetwrapper.o \
		./build/skdatetime.o \
		./build/skfltkui.o \
		./build/skapp.o \
		./build/skobject.o \
		./build/skdefines.o \
		./build/skstringlist.o \
		./build/skstring.o \
		./build/skosenv.o \
		./build/sktempfile.o \
		./build/skfileinfoslist.o \
		./build/skfsutils.o \
		./build/skargsmap.o \
		./build/sklogmachine.o \
		./build/skvariant.o \
		./build/skflatobject.o \
		./build/skarraycast.o \
		./build/robotvideocapture.o \
		./build/main.o

all: Makefile RobotVideoCapture.bin

clean: 
	-$(DEL_FILE) $(OBJECTS)
distclean: clean 
	-$(DEL_FILE) $(OBJECTS) 
	-$(DEL_FILE) $(TARGET) 
	-$(DEL_FILE) ./Makefile

RobotVideoCapture.bin: $(OBJECTS)
	$(LINK) $(OBJECTS) $(LIBS) -o $(TARGET)

./build/skobject.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skobject.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skattach.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/sksignal.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skslot.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skargsmap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Thread/skmutexlocker.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/sklogmachine.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstringlist.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/sklinkedmap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/sklist.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarray.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/sktreemap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skapp.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvariant.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skobject.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skobject.cpp

./build/skhttpresponseheaders.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpresponseheaders.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skargsmap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpstatuscodes.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpresponseheaders.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skabstractdevice.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Containers/HTML/skwebpage.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/skcookies.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skhttpresponseheaders.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpresponseheaders.cpp

./build/robotvideocapture.o: \
		/home/roko/Scrivania/develop/robot-video-capture/robotvideocapture.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowsat.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Core/System/Network/FlowNetwork/skflowvideopublisher.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skimagecapture.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skcli.h \
		/home/roko/Scrivania/develop/robot-video-capture/robotvideocapture.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/robotvideocapture.o /home/roko/Scrivania/develop/robot-video-capture/robotvideocapture.cpp

./build/sktcpserver.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/sktcpserver.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/skabstractserver.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/TCP/skflattcpserver.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/sktcpsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/sktcpserver.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skapp.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/sktcpserver.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/sktcpserver.cpp

./build/skcookies.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/skcookies.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skargsmap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Time/skdatetime.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarray.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/skcookies.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skcookies.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/skcookies.cpp

./build/skelapsedtime.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Time/skelapsedtime.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstring.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Time/skelapsedtime.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvariant.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skelapsedtime.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Time/skelapsedtime.cpp

./build/skcli.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skcli.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/sktreemap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstringlist.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvariant.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skcli.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/sklogmachine.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skcli.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skcli.cpp

./build/skflatlocalsocket.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/LOCAL/skflatlocalsocket.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/skabstractflatsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/LOCAL/skflatlocalsocket.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skflatlocalsocket.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/LOCAL/skflatlocalsocket.cpp

./build/skurl.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/skurl.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skargsmap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/skurl.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/sklogmachine.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skurl.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/skurl.cpp

./build/skwsredistr.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/skwsredistr.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skdeviceredistr.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/skwebsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/skwsredistr.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skwsredistr.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/skwsredistr.cpp

./build/skabstractflowpublisher.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skabstractflowpublisher.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowasync.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/HTTP/skflowasynchttpservice.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skabstractflowpublisher.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skrawredistrmountpoint.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skabstractflowpublisher.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skabstractflowpublisher.cpp

./build/skpsyvenc.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/Encoders/Magma/skpsyvenc.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Multimedia/Magma/skabstractmagmaencoder.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Multimedia/Magma/skabstractmagmadecoder.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skimageutils.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skimagedecoder.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/Encoders/Magma/skpsyvenc.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skeventloop.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skbufferdevice.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skpsyvenc.o /home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/Encoders/Magma/skpsyvenc.cpp

./build/skapp.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skapp.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skeventloop.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skcli.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Filesystem/skmimetype.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Thread/skthread.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skargsmap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skapp.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skapp.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/sklogmachine.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skosenv.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltkui.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvariant.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skapp.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skapp.cpp

./build/skflowasync.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowasync.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowsync.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skset.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowasync.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skabstractworkerobject.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skflowasync.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowasync.cpp

./build/skringbuffer.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skringbuffer.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstring.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skringbuffer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skdatabuffer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvariant.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/sklogmachine.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skringbuffer.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skringbuffer.cpp

./build/skabstractflowsat.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skabstractflowsat.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skapp.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skabstractworkerobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowasync.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skabstractflowpublisher.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skabstractflowsubscriber.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/HTTP/skflowasynchttpservice.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skabstractflowsat.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skosenv.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skabstractflowsat.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skabstractflowsat.cpp

./build/skmutexlocker.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Thread/skmutexlocker.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Thread/skmutexlocker.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skmutexlocker.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Thread/skmutexlocker.cpp

./build/skstringlist.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstringlist.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstring.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstringlist.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvariant.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skstringlist.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstringlist.cpp

./build/skdefines.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/skdefines.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/skdefines.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/sklogmachine.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skargsmap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvariant.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstringlist.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skdefines.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/skdefines.cpp

./build/skabstractflatsocket.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/skabstractflatsocket.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skabstractflatdevice.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/skabstractflatsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skringbuffer.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skabstractflatsocket.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/skabstractflatsocket.cpp

./build/skfltkwindows.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltkwindows.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltkgroups.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltklayout.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltkwindows.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltkui.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skapp.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skfltkwindows.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltkwindows.cpp

./build/sksignal.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/sksignal.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skattach.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvariant.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/sksignal.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skslot.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skapp.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/sksignal.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/sksignal.cpp

./build/skdatabuffer.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skdatabuffer.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstring.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skdatacrypt.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skdatabuffer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skringbuffer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvariant.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/sklogmachine.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/skmath.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skdatabuffer.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skdatabuffer.cpp

./build/skabstractmagmadecoder.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Multimedia/Magma/skabstractmagmadecoder.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Multimedia/Magma/skabstractmagmaencoder.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Multimedia/Magma/skabstractmagmadecoder.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skbufferdevice.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skabstractmagmadecoder.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Multimedia/Magma/skabstractmagmadecoder.cpp

./build/skfsutils.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skdatacrypt.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skosenv.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Filesystem/skflatfile.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skapp.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Filesystem/skfile.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Filesystem/skfileinfoslist.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skargsmap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skfsutils.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.cpp

./build/skmat.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skmat.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skimageutils.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skmat.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/sklogmachine.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstringlist.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skmat.o /home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skmat.cpp

./build/skwuielement.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Containers/HTML/skwuielement.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Containers/XML/skxmlwriter.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skargsmap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Containers/HTML/skwuielement.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skwuielement.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Containers/HTML/skwuielement.cpp

./build/skfileinfoslist.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Filesystem/skfileinfoslist.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Filesystem/sktempfile.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Filesystem/skfileinfoslist.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skfileinfoslist.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Filesystem/skfileinfoslist.cpp

./build/skpartredistrmountpoint.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skpartredistrmountpoint.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skrawredistrmountpoint.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skpartredistrmountpoint.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/skmultipartredistr.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skpartredistrmountpoint.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skpartredistrmountpoint.cpp

./build/skdatetime.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Time/skdatetime.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstring.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Time/skdatetime.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/sklogmachine.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvariant.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skdatetime.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Time/skdatetime.cpp

./build/skstandalonesignal.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skstandalonesignal.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skstandalonesignal.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skstandalonesignal.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skstandalonesignal.cpp

./build/skabstractworkerobject.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skabstractworkerobject.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skabstractworkerobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skeventloop.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skabstractdevice.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skabstractworkerobject.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skabstractworkerobject.cpp

./build/skslot.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skslot.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skpair.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/sksignal.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skslot.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvariant.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skslot.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skslot.cpp

./build/skflowprotocol.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowprotocol.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/skabstractsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skbufferdevice.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/FlowNetwork/skflowcommon.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowprotocol.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/sktreemap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skflowprotocol.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowprotocol.cpp

./build/skflattcpsocket.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/TCP/skflattcpsocket.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/skabstractflatsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/TCP/skflattcpsocket.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skflattcpsocket.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/TCP/skflattcpsocket.cpp

./build/skfltkwidgetwrapper.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltkwidgetwrapper.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skmat.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltkwidgetwrapper.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltkwindows.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltkui.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skfltkwidgetwrapper.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltkwidgetwrapper.cpp

./build/skflowsync.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowsync.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skabstractflow.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowsync.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/LOCAL/sklocalsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/sktcpsocket.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skflowsync.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowsync.cpp

./build/skargsmap.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skargsmap.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvariant.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skmap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstringlist.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skargsmap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skargsmap.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skargsmap.cpp

./build/skflowvideopublisher.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Core/System/Network/FlowNetwork/skflowvideopublisher.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skimagecapture.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skimageencoder.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skmat.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Multimedia/Magma/skmagmawriter.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skwhitebalancer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Core/System/Network/FlowNetwork/skflowvideomjpegpublisher.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Core/System/Network/FlowNetwork/skflowvideomagmapublisher.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Core/System/Network/FlowNetwork/skflowvideopublisher.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skeventloop.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skpartredistrmountpoint.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skflowvideopublisher.o /home/roko/Scrivania/develop/SpecialK/LibSkVision/Core/System/Network/FlowNetwork/skflowvideopublisher.cpp

./build/skimagedecoder.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skimagedecoder.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstring.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skimagedecoder.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/sklogmachine.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skimagedecoder.o /home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skimagedecoder.cpp

./build/skabstractflow.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skabstractflow.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowprotocol.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skabstractflow.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowsync.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/LOCAL/sklocalsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/sktcpsocket.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skabstractflow.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skabstractflow.cpp

./build/skflowcommon.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/FlowNetwork/skflowcommon.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/sktreemap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/FlowNetwork/skflowcommon.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skflowcommon.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/FlowNetwork/skflowcommon.cpp

./build/skdatacrypt.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skdatacrypt.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstring.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skdatacrypt.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/sklogmachine.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skdatabuffer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skargsmap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Time/skdatetime.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skdatacrypt.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skdatacrypt.cpp

./build/skmagmawriter.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Multimedia/Magma/skmagmawriter.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Multimedia/Magma/skabstractmagmaencoder.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Multimedia/Magma/skmagmawriter.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skbufferdevice.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skmagmawriter.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Multimedia/Magma/skmagmawriter.cpp

./build/skimageutils.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skimageutils.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skimageutils.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/sklogmachine.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skimageutils.o /home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skimageutils.cpp

./build/skwsredistrmountpoint.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skwsredistrmountpoint.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skredistrmountpoint.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/skwebsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skwsredistrmountpoint.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/skwsredistr.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skwsredistrmountpoint.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skwsredistrmountpoint.cpp

./build/skabstractflatdevice.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skabstractflatdevice.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstring.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Object/skflatsignal.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skabstractflatdevice.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skringbuffer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skdatabuffer.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skabstractflatdevice.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skabstractflatdevice.cpp

./build/skfltkui.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltkui.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltkwidgetwrapper.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/sktreemap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltkui.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skapp.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Time/skdatetime.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skfltkui.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltkui.cpp

./build/skabstractflatserver.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/skabstractflatserver.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/skabstractflatsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skqueue.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/skabstractflatserver.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skabstractflatserver.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/skabstractflatserver.cpp

./build/skmultipartredistr.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/skmultipartredistr.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skdeviceredistr.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpresponseheaders.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/skcookies.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/skmultipartredistr.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skmultipartredistr.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/skmultipartredistr.cpp

./build/skhttpstatuscodes.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpstatuscodes.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/skdefines.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpstatuscodes.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skhttpstatuscodes.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpstatuscodes.cpp

./build/skflowgenericpublisher.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowgenericpublisher.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skabstractflowpublisher.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowgenericpublisher.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skrawredistrmountpoint.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skflowgenericpublisher.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowgenericpublisher.cpp

./build/skpostparser.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skpostparser.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skmap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstringlist.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstring.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skpostparser.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/sklogmachine.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skpostparser.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skpostparser.cpp

./build/skflowasynchttpservice.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/HTTP/skflowasynchttpservice.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowasync.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/HTTP/skflowasynchttpservice.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skdeviceredistr.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpservice.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skfsmountpoint.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skpartredistrmountpoint.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skeventloop.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skflowasynchttpservice.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/HTTP/skflowasynchttpservice.cpp

./build/sklogmachine.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/sklogmachine.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skqueue.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/sklogmachine.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvariant.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skapp.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Thread/skthread.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Time/skdatetime.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skargsmap.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/sklogmachine.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/sklogmachine.cpp

./build/skmimetype.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Filesystem/skmimetype.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstring.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Filesystem/skmimetype.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skapp.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skmimetype.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Filesystem/skmimetype.cpp

./build/skabstractmagmaencoder.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Multimedia/Magma/skabstractmagmaencoder.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skdatabuffer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Multimedia/Magma/skabstractmagmaencoder.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skbufferdevice.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skabstractmagmaencoder.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Multimedia/Magma/skabstractmagmaencoder.cpp

./build/skthread.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Thread/skthread.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skeventloop.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Thread/skthread.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skapp.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skthread.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Thread/skthread.cpp

./build/skflatsignal.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Object/skflatsignal.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/sktreemap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Object/skflatsignal.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/sklogmachine.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skflatsignal.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Object/skflatsignal.cpp

./build/skfltklayout.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltklayout.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltkwidgetwrapper.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltklayout.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltkui.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skfltklayout.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltklayout.cpp

./build/skflattcpserver.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/TCP/skflattcpserver.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/skabstractflatserver.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/TCP/skflattcpserver.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skflattcpserver.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/TCP/skflattcpserver.cpp

./build/sksslsocket.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/SSL/sksslsocket.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/skabstractsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skringbuffer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/SSL/sksslsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skeventloop.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/sknetutils.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/sksslsocket.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/SSL/sksslsocket.cpp

./build/skflowvideomagmapublisher.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Core/System/Network/FlowNetwork/skflowvideomagmapublisher.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowgenericpublisher.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skmat.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Multimedia/Magma/skmagmawriter.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/Encoders/Magma/skpsyvenc.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Core/System/Network/FlowNetwork/skflowvideomagmapublisher.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skeventloop.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skpartredistrmountpoint.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skflowvideomagmapublisher.o /home/roko/Scrivania/develop/SpecialK/LibSkVision/Core/System/Network/FlowNetwork/skflowvideomagmapublisher.cpp

./build/sktcpsocket.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/sktcpsocket.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/skabstractsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/TCP/skflattcpsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/skipaddress.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/sktcpsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstring.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/sktcpsocket.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/sktcpsocket.cpp

./build/skstring.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstring.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/skdefines.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvector.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstring.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvariant.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstringlist.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skstring.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstring.cpp

./build/skrawredistrmountpoint.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skrawredistrmountpoint.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skredistrmountpoint.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skringbuffer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skqueue.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skrawredistrmountpoint.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skdeviceredistr.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpresponseheaders.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpsocket.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skrawredistrmountpoint.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skrawredistrmountpoint.cpp

./build/skbufferdevice.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skbufferdevice.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skabstractdevice.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skdatabuffer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skbufferdevice.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skbufferdevice.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skbufferdevice.cpp

./build/skabstractsocket.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/skabstractsocket.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skabstractdevice.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/skabstractflatsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/skabstractsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skapp.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skringbuffer.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skabstractsocket.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/skabstractsocket.cpp

./build/main.o: \
		/home/roko/Scrivania/develop/robot-video-capture/main.cpp \
		/home/roko/Scrivania/develop/robot-video-capture/robotvideocapture.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/main.o /home/roko/Scrivania/develop/robot-video-capture/main.cpp

./build/skimagecapture.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skimagecapture.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skimageutils.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skimagecapture.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skeventloop.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skimagecapture.o /home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skimagecapture.cpp

./build/skwebsocket.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/skwebsocket.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skringbuffer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skqueue.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/skwebsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skeventloop.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skbufferdevice.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpresponseheaders.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skwebsocket.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/skwebsocket.cpp

./build/skarraycast.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skarraycast.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.cpp

./build/skabstractserver.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/skabstractserver.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skqueue.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/skabstractflatserver.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/skabstractserver.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skapp.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/skabstractsocket.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skabstractserver.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/skabstractserver.cpp

./build/skgenericmountpoint.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skgenericmountpoint.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpstatuscodes.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skgenericmountpoint.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skeventloop.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/skwebsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/skurl.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skgenericmountpoint.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skgenericmountpoint.cpp

./build/skfsmountpoint.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skfsmountpoint.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skgenericmountpoint.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skfsmountpoint.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Filesystem/skfile.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpsocket.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skfsmountpoint.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skfsmountpoint.cpp

./build/skattach.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skattach.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skattach.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skobject.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skattach.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skattach.cpp

./build/sksslserver.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/SSL/sksslserver.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/sktcpserver.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/SSL/sksslserver.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvariant.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/SSL/sksslsocket.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/sksslserver.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/SSL/sksslserver.cpp

./build/skwhitebalancer.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skwhitebalancer.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstring.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skwhitebalancer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/sklogmachine.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skwhitebalancer.o /home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skwhitebalancer.cpp

./build/skxmlwriter.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Containers/XML/skxmlwriter.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skabstractdevice.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Containers/XML/skxmlwriter.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skargsmap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skapp.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skxmlwriter.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Containers/XML/skxmlwriter.cpp

./build/skflatobject.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Object/skflatobject.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/skdefines.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/sklogmachine.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvariant.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skflatobject.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Object/skflatobject.cpp

./build/skfltkgroups.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltkgroups.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltkwidgetwrapper.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltkgroups.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltkui.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skfltkgroups.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/UI/FLTK/skfltkgroups.cpp

./build/skflowsat.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowsat.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skabstractflowsat.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowsat.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skflowsat.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowsat.cpp

./build/sktempfile.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Filesystem/sktempfile.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Filesystem/skfile.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Filesystem/sktempfile.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skosenv.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/sktempfile.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Filesystem/sktempfile.cpp

./build/skredistrmountpoint.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skredistrmountpoint.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skgenericmountpoint.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skredistrmountpoint.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skdatabuffer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skdeviceredistr.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpsocket.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skredistrmountpoint.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skredistrmountpoint.cpp

./build/skflatfile.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Filesystem/skflatfile.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skabstractflatdevice.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skdatabuffer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Filesystem/skflatfile.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skflatfile.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Filesystem/skflatfile.cpp

./build/sklocalsocket.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/LOCAL/sklocalsocket.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/skabstractsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/LOCAL/skflatlocalsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/LOCAL/sklocalsocket.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/sklocalsocket.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/LOCAL/sklocalsocket.cpp

./build/skwebpage.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Containers/HTML/skwebpage.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Containers/HTML/skwuielement.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/skcookies.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Containers/HTML/skwebpage.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Containers/XML/skxmlwriter.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skbufferdevice.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skwebpage.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Containers/HTML/skwebpage.cpp

./build/skflowvideomjpegpublisher.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Core/System/Network/FlowNetwork/skflowvideomjpegpublisher.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowgenericpublisher.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skmat.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skimageencoder.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Core/System/Network/FlowNetwork/skflowvideomjpegpublisher.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skeventloop.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skpartredistrmountpoint.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skflowvideomjpegpublisher.o /home/roko/Scrivania/develop/SpecialK/LibSkVision/Core/System/Network/FlowNetwork/skflowvideomjpegpublisher.cpp

./build/skabstractdevice.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skabstractdevice.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skabstractflatdevice.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skabstractdevice.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skapp.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvariant.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skringbuffer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skdatabuffer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skabstractdevice.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skabstractdevice.cpp

./build/skabstractflowsubscriber.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skabstractflowsubscriber.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skflowasync.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skabstractflowsubscriber.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skabstractflowsubscriber.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/FlowNetwork/skabstractflowsubscriber.cpp

./build/skdeviceredistr.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skdeviceredistr.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skabstractdevice.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skdatabuffer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skargsmap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skdeviceredistr.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skringbuffer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/skabstractsocket.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skdeviceredistr.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skdeviceredistr.cpp

./build/skvariant.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvariant.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstring.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstack.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skqueue.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/abstract/skabstractlist.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skdatabuffer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skcpointer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skmap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvariant.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skringbuffer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skargsmap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skarraycast.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/sklogmachine.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/sklist.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvariant.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skvariant.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvariant.cpp

./build/skipaddress.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/skipaddress.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstring.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/skipaddress.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/sknetutils.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvariant.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skipaddress.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/skipaddress.cpp

./build/skeventloop.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skeventloop.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Time/skelapsedtime.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skstandalonesignal.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skqueue.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skeventloop.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skapp.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Thread/skthread.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Object/skstandalonesignal.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skdatacrypt.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skeventloop.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skeventloop.cpp

./build/skfile.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Filesystem/skfile.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/skdefines.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstring.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/skabstractdevice.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skdatabuffer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Filesystem/skflatfile.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Filesystem/skfile.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skfile.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Filesystem/skfile.cpp

./build/skhttpsocket.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpsocket.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skringbuffer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skdatabuffer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/sktcpsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/SSL/sksslsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Time/skelapsedtime.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpresponseheaders.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skpostparser.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/Containers/HTML/skwebpage.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/skcookies.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/skurl.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skgenericmountpoint.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skeventloop.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/skurl.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Filesystem/skfile.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Time/skdatetime.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skdatabuffer.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/LOCAL/sklocalsocket.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skhttpsocket.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpsocket.cpp

./build/skhttpservice.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpservice.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/SSL/sksslserver.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpstatuscodes.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skgenericmountpoint.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpservice.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpsocket.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skfsmountpoint.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skrawredistrmountpoint.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skpartredistrmountpoint.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/Mountpoints/skwsredistrmountpoint.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/skcookies.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skhttpservice.o /home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/System/Network/TCP/HTTP/Server/skhttpservice.cpp

./build/skimageencoder.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skimageencoder.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstring.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skimageencoder.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/sklogmachine.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skimageencoder.o /home/roko/Scrivania/develop/SpecialK/LibSkVision/Multimedia/Image/skimageencoder.cpp

./build/skmath.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/skmath.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Object/skflatobject.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/skmath.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skmath.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/skmath.cpp

./build/sknetutils.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/sknetutils.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/skipaddress.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/sknetutils.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstringlist.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/sklogmachine.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skvariant.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/sknetutils.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Network/sknetutils.cpp

./build/skosenv.o: \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skosenv.cpp \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/sktreemap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstringlist.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skosenv.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkCore/Core/App/skapp.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skstring.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/Containers/skargsmap.h \
		/home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/Filesystem/skfsutils.h
	@mkdir -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o ./build/skosenv.o /home/roko/Scrivania/develop/SpecialK/LibSkFlat/Core/System/skosenv.cpp

