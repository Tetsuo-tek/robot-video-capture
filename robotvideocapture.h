#ifndef ROBOTVIDEOCAPTURE_H
#define ROBOTVIDEOCAPTURE_H

#include <Core/System/Network/FlowNetwork/skflowsat.h>
#include <Core/System/Network/FlowNetwork/skflowvideopublisher.h>
#include <Multimedia/Image/skimagecapture.h>
#include <Core/System/skcli.h>

//JETSON NANO CAM
//--gstreamer-source "nvarguscamerasrc sensor-id=0 ! video/x-raw(memory:NVMM), width=(int)1280, height=(int)720, framerate=(fraction)30/1 ! nvvidconv flip-method=2 ! video/x-raw, width=(int)1280, height=(int)720, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink"

class RobotVideoCapture extends SkFlowSat
{
    int capID;
    int compression;
    SkString gStreamerSource;

    public:
        Constructor(RobotVideoCapture, SkFlowSat);

    protected:
        SkImageCapture *vc;
        SkVideoCaptureProperties props;

        SkMat f;
        SkFlowVideoPublisher *publisher;

    private:
        bool onSetup()              override;
        void onInit()               override;
        void onQuit()               override;

        void onFastTick()           override;
};

#endif // ROBOTVIDEOCAPTURE_H
